import { vi } from "vitest";

export const notifySpy = vi.fn();

export function useNotification() {
  return {
    notify: notifySpy,
  };
}
