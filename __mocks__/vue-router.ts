import { vi } from "vitest";

export const useRoute = () => {
  return {
    fullPath: "",
    name: "",
  };
};

export const useRouter = () => {
  return {
    fullPath: "",
    hash: "",
    matched: [],
    meta: "",
    name: "",
    params: "",
    path: "",
    query: "",
    redirectedFrom: "",
    push: pushSpy,
  };
};

export const pushSpy = vi.fn();
