# trident

## <img src="./assets/fire-icon.svg" width="25" height="20"> Framework

- Vue.js <img src="./assets/vue-icon.svg" width="25" height="20">
- Vitest <img src="./assets/test-icon.svg" width="25" height="20">
- Tailwindcss <img src="./assets/css-icon.svg" width="25" height="20">
- Eslint <img src="./assets/eslint-icon.svg" width="25" height="20">
