import { defineStore } from "pinia";
import { computed, ref } from "vue";
import { type User } from "@/types/User";

export const useUserStore = defineStore("userStore", () => {
  const user = ref(JSON.parse(localStorage.getItem("user") as string));

  function setUser(newUser: User | null) {
    user.value = newUser;
    localStorage.setItem("user", JSON.stringify(newUser));
  }

  function removeUser() {
    localStorage.removeItem("user");
    setUser(null);
  }

  const getUser = computed(() => {
    return user.value;
  });

  const getToken = computed(() => {
    return user.value.token;
  });

  const getRole = computed(() => {
    return user.value.role;
  });

  const getLastname = computed(() => {
    return user.value.lastname;
  });

  const getName = computed(() => {
    return user.value.name;
  });

  const getEmail = computed(() => {
    return user.value.email;
  });

  const getExpirationDate = computed(() => {
    return user.value.expirationDate;
  });

  const getFullName = computed(() => {
    return `${user.value.name} ${user.value.lastname}`;
  });

  const isAuthenticated = computed(() => {
    if (user.value) {
      return true;
    } else {
      return false;
    }
  });

  return {
    getUser,
    setUser,
    getToken,
    getRole,
    getLastname,
    getName,
    getFullName,
    isAuthenticated,
    removeUser,
    getEmail,
    getExpirationDate,
  };
});
