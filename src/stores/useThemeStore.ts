import { defineStore } from "pinia";
import { ref } from "vue";

type Theme = "light" | "dark";

export const useThemeStore = defineStore("themeStore", () => {
  const theme = ref(localStorage.getItem("theme"));

  function switchTheme() {
    const newTheme = theme.value === "dark" ? "light" : "dark";
    switchStorageTheme(newTheme);
    theme.value = newTheme;
  }

  function switchStorageTheme(newTheme: Theme) {
    localStorage.setItem("theme", newTheme);
  }

  return { theme, switchTheme, switchStorageTheme };
});
