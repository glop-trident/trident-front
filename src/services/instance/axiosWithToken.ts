import axios from "axios";
import { useUserStore } from "@/stores/useUserStore";
import useNotifications from "@/composables/useNotifications";

const instance = axios.create({
  baseURL: `${import.meta.env.VITE_API_URL}`,
  headers: {
    "Content-Type": "application/json",
  },
});

instance.interceptors.request.use((config) => {
  const userStore = useUserStore();
  const token = userStore.getToken;

  if (token) {
    config.headers["Authorization"] = `Bearer ${token}`;
  }

  return config;
});

instance.interceptors.response.use(
  (response) => response,
  async (error) => {
    const userStore = useUserStore();
    const { errorNotification } = useNotifications();

    if (error.response && error.response.status === 403) {
      userStore.removeUser();
      window.location.href = "/login";
      errorNotification("Votre session a expirée");
    }

    throw error;
  },
);

export default instance;
