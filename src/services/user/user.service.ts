import axios from "@/services/instance/axiosWithToken";
import { GetUserResponse } from "@/services/user/types/GetUserResponse";

export const getUser = (email: string): Promise<GetUserResponse> => {
  return axios.get(`/client?email=${email}`);
};
