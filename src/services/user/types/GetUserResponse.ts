import { Role } from "@/types/Role";

export type GetUserResponse = {
  data: {
    email: string;
    name: string;
    lastName: string;
    role: Role;
    image: string;
    zipCode: string;
    birthday: Date;
    city: string;
    phoneNumber: string;
  };
};
