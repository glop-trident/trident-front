import axios from "@/services/instance/axiosWithoutToken";
import { RegisterForm } from "@/components/authentification/register/types/RegisterForm";
import { LoginForm } from "@/components/authentification/login/types/LoginForm";
import { LoginUserResponse } from "@/services/authentification/types/LoginUserResponse";
import { CreateUserResponse } from "@/services/authentification/types/CreateUserResponse";

export const createUser = (data: RegisterForm): Promise<CreateUserResponse> => {
  return axios.post("/user", data);
};

export const loginUser = (data: LoginForm): Promise<LoginUserResponse> => {
  return axios.post("/login", data);
};
