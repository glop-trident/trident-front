export type LoginUserResponse = {
  data: {
    accessToken: string;
  };
};
