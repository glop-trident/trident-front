import { Role } from "@/types/Role";

export type CreateUserResponse = {
  data: {
    name: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    city: string;
    zipCode: number;
    birthday: Date;
    image: string;
    role: Role;
    address: string;
    category: string;
    note: string;
    pub: boolean;
    siretCode: number;
  };
};
