import { decode } from "jsonwebtoken-esm";

export async function decodeToken(token: string) {
  try {
    const decodedToken = decode(token);
    return decodedToken;
  } catch (error) {
    console.error("Erreur lors du déchiffrement du token :", error);
  }
}
