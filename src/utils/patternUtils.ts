const phonePattern = "^0[1-9](\\d{2}){4}$";
const frenchZipcodePattern = "^(0[1-9]|[1-8][0-9]|9[0-8])\\d{3}$";
const siretPattern = "^[0-9]{14}$";

export { phonePattern, frenchZipcodePattern, siretPattern };
