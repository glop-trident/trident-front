const todayDate = new Date().toISOString().split("T")[0];

function formatDate(date: Date): string {
  return date.toISOString().split("T")[0];
}

export { todayDate, formatDate };
