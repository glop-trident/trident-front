import { useNotification } from "@kyvg/vue3-notification";

const useNotifications = () => {
  const { notify } = useNotification();

  const successNotification = (message: string) => {
    notify({
      type: "success",
      title: "Succès",
      text: message,
    });
  };

  const errorNotification = (message: string) => {
    notify({
      type: "error",
      title: "Erreur",
      text: message,
    });
  };

  return {
    errorNotification,
    successNotification,
  };
};

export default useNotifications;
