interface TabHeader {
  id: string;
  label: string;
  icon: string;
}

export type { TabHeader };
