export type NavigationContent = {
  title: string;
  icon?: string;
  routePath: string;
  available: boolean;
};
