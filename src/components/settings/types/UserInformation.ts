import { Role } from "@/types/Role";

export type UserInformation = {
  email: string;
  name: string;
  lastName: string;
  role: Role | string;
  image: string;
  zipCode: number | string;
  birthday: Date | string;
  city: string;
  phoneNumber: string;
};
