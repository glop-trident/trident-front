export type CreditCardInformation = {
  owner: string;
  cardNumber: number | string;
  crypto: number | string;
  expirationDate: Date | string;
};
