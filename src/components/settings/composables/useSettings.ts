import { onMounted, reactive, ref } from "vue";
import { getUser } from "@/services/user/user.service";
import { useUserStore } from "@/stores/useUserStore";
import { UserInformation } from "@/components/settings/types/UserInformation";
import { CreditCardInformation } from "@/components/settings/types/CreditCardInformation";
import { formatDate } from "@/utils/dateUtils";

export function useSettings() {
  const userStore = useUserStore();

  const password = ref("");

  const user: UserInformation = reactive({
    email: "",
    name: "",
    lastName: "",
    role: "",
    image: "",
    zipCode: "",
    birthday: new Date(),
    city: "",
    phoneNumber: "",
  });

  const userBase: UserInformation = reactive({
    email: "",
    name: "",
    lastName: "",
    role: "",
    image: "",
    zipCode: "",
    birthday: new Date(),
    city: "",
    phoneNumber: "",
  });

  const creditCard: CreditCardInformation = reactive({
    owner: "",
    cardNumber: "",
    expirationDate: new Date(),
    crypto: "",
  });

  const creditCardBase: CreditCardInformation = reactive({
    owner: "",
    cardNumber: "",
    expirationDate: new Date(),
    crypto: "",
  });

  onMounted(() => {
    loadSettings();
  });

  const loadSettings = async () => {
    await getUser(userStore.getEmail).then((response) => {
      user.email = response.data.email;
      user.role = response.data.role;
      user.lastName = response.data.lastName;
      user.name = response.data.name;
      user.birthday = formatDate(new Date(response.data.birthday));
      user.city = response.data.city;
      user.phoneNumber = response.data.phoneNumber;
      user.zipCode = response.data.zipCode;
      user.image = response.data.image;

      userBase.email = user.email;
      userBase.role = user.role;
      userBase.lastName = user.lastName;
      userBase.name = user.name;
      userBase.birthday = user.birthday;
      userBase.city = user.city;
      userBase.phoneNumber = user.phoneNumber;
      userBase.zipCode = user.zipCode;
      userBase.image = user.image;
    });
  };

  function resetUserSettings() {
    user.email = userBase.email;
    user.role = userBase.role;
    user.lastName = userBase.lastName;
    user.name = userBase.name;
    user.birthday = userBase.birthday;
    user.city = userBase.city;
    user.phoneNumber = userBase.phoneNumber;
    user.zipCode = userBase.zipCode;
    user.image = userBase.image;
  }

  function resetCreditCardSettings() {
    creditCard.owner = creditCardBase.owner;
    creditCard.cardNumber = creditCardBase.cardNumber;
    creditCard.expirationDate = creditCardBase.expirationDate;
    creditCard.crypto = creditCardBase.crypto;
  }

  return {
    user,
    creditCard,
    loadSettings,
    resetUserSettings,
    resetCreditCardSettings,
    password,
  };
}
