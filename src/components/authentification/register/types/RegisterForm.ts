export type RegisterForm = {
  name?: string;
  lastName?: string;
  email: string;
  phoneNumber: string;
  city: string;
  zipCode: string;
  birthday?: Date;
  password: string;
  pub?: boolean;
  address?: string;
  siretCode?: string;
  role?: string;
};
