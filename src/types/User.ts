import { type Role } from "@/types/Role";

export type User = {
  token: string;
  role: Role;
  lastname: string;
  name: string;
  email: string;
  expirationDate: Date;
};
