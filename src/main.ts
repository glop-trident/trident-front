import { createApp } from "vue";
import router from "@/router/index";
import App from "@/App.vue";
import { createPinia } from "pinia";
import "@/style.css";

import VueClickAway from "vue3-click-away";
import FloatingVue from "floating-vue";

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import "@fortawesome/fontawesome-free/css/all.css";
import "floating-vue/dist/style.css";
import Notifications from "@kyvg/vue3-notification";

const app = createApp(App);
const pinia = createPinia();

app.component("font-awesome-icon", FontAwesomeIcon);

app.use(VueClickAway);
app.use(FloatingVue);
app.use(router);
app.use(pinia);
app.use(Notifications);

app.mount("#app");
