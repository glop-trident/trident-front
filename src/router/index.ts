import { createRouter, createWebHistory } from "vue-router";
import { useUserStore } from "@/stores/useUserStore";

const HomeView = () => import("@/views/HomeView.vue");
const LoginView = () => import("@/views/LoginView.vue");
const StoreView = () => import("@/views/StoreView.vue");
const ErrorView = () => import("@/views/ErrorView.vue");
const SettingsView = () => import("@/views/SettingsView.vue");

// REGISTER VIEW
const RegisterView = () => import("@/views/register/RegisterView.vue");
const MarketRegisterView = () =>
  import("@/views/register/MarketRegisterView.vue");
const TerritoryRegisterView = () =>
  import("@/views/register/TerritoryRegisterView.vue");

const routes = [
  {
    path: "/",
    redirect: { name: "home" },
  },
  {
    path: "/home",
    name: "home",
    component: HomeView,
    meta: {
      requiresAuth: true,
      roles: ["admin", "client", "merchant", "city"],
    },
  },
  {
    path: "/login",
    name: "login",
    component: LoginView,
  },

  {
    path: "/settings",
    name: "settings",
    meta: {
      requiresAuth: true,
      roles: ["admin", "client", "merchant", "city"],
    },
    component: SettingsView,
  },

  {
    path: "/register",
    name: "register",
    component: RegisterView,
  },
  {
    path: "/store",
    name: "store",
    component: StoreView,
    meta: {
      requiresAuth: true,
      roles: ["admin", "client", "merchant", "city"],
    },
  },
  {
    path: "/error",
    name: "error",
    component: ErrorView,
    meta: {
      requiresAuth: true,
      roles: ["admin", "client", "merchant", "city"],
    },
  },
  {
    path: "/invite/merchant",
    name: "marketregister",
    component: MarketRegisterView,
  },
  {
    path: "/invite/territory",
    name: "territoryregister",
    component: TerritoryRegisterView,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  const userStore = useUserStore();
  const isAuthenticated = userStore.isAuthenticated;

  if (to.meta.requiresAuth && !isAuthenticated) {
    next({ name: "login" });
  } else if (
    !to.meta.requiresAuth &&
    !isAuthenticated &&
    to.name !== "login" &&
    to.name !== "register" &&
    to.name !== "marketregister" &&
    to.name !== "territoryregister"
  ) {
    next({ name: "login" });
  } else if (!to.meta.requiresAuth && isAuthenticated) {
    next({ name: "error" });
  } else {
    next();
  }
});

export default router;
