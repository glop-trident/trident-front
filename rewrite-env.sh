#!/bin/sh

INIT=$(pwd)
cd "$(dirname "$0")"

CONTENT=$(cat .env | sed \
"s+BACKEND_BASE_URL_PLACEHOLDER+$BACKEND_BASE_URL+g" \
)

echo $CONTENT > .env

cd $INIT
