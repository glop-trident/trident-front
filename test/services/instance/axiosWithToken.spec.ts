import { expect, it, describe } from "vitest";
import axiosInstance from "../../../src/services/instance/axiosWithToken";

describe("axiosInstance", () => {
  it("should have the correct baseURL", () => {
    expect(axiosInstance.defaults.baseURL).toBe("http://localhost:8080");
  });

  it("should have the correct Content-Type header", () => {
    expect(axiosInstance.defaults.headers["Content-Type"]).toBe(
      "application/json",
    );
  });
});
