import { expect, it, describe, vi } from "vitest";
import {
  createUser,
  loginUser,
} from "../../../src/services/authentification/auth.service";
import axiosInstance from "../../../src/services/instance/axiosWithoutToken";
import { Axios } from "axios";

vi.mock("../../../src/services/instance/axiosWithoutToken", async () => {
  const axios: Axios = await vi.importActual(
    "../../../src/services/instance/axiosWithoutToken",
  );

  return { ...axios, post: vi.fn() };
});

describe("auth.service", () => {
  describe("createUser", () => {
    it("should make a POST request to /user with the provided data", async () => {
      const data = {};

      axiosInstance.post = vi
        .fn()
        .mockResolvedValueOnce({ data: "response data" });

      const response = await createUser(data);

      expect(axiosInstance.post).toHaveBeenCalledWith("/user", data);
      expect(response.data).toBe("response data");
    });

    it("should handle errors", async () => {
      const data = {};

      axiosInstance.post = vi
        .fn()
        .mockRejectedValueOnce(new Error("Error message"));

      try {
        await createUser(data);
      } catch (error) {
        expect(error.message).toBe("Error message");
      }
    });
  });

  describe("loginUser", () => {
    it("should make a POST request to /login with the provided data", async () => {
      const data = {};

      axiosInstance.post = vi
        .fn()
        .mockResolvedValueOnce({ data: "response data" });

      const response = await loginUser(data);

      expect(axiosInstance.post).toHaveBeenCalledWith("/login", data);
      expect(response.data).toBe("response data");
    });

    it("should handle errors", async () => {
      const data = {};

      axiosInstance.post = vi
        .fn()
        .mockRejectedValueOnce(new Error("Error message"));

      try {
        await loginUser(data);
      } catch (error) {
        expect(error.message).toBe("Error message");
      }
    });
  });
});
