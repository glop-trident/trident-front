import { expect, it, describe, vi } from "vitest";
import useNotifications from "../../src/composables/useNotifications";
import { notifySpy } from "../../__mocks__/@kyvg/vue3-notification";

vi.mock("@kyvg/vue3-notification");

describe("useNotifications", () => {
  describe("successNotification", () => {
    it("should show success notification", () => {
      const { successNotification } = useNotifications();

      successNotification("Test Success Message");

      expect(notifySpy).toHaveBeenCalledWith({
        type: "success",
        title: "Succès",
        text: "Test Success Message",
      });
    });
  });

  describe("errorNotification", () => {
    it("should show error notification", () => {
      const { errorNotification } = useNotifications();

      errorNotification("Test Error Message");

      expect(notifySpy).toHaveBeenCalledWith({
        type: "error",
        title: "Erreur",
        text: "Test Error Message",
      });
    });
  });
});
