import { beforeEach, expect, it, describe } from "vitest";
import { setActivePinia, createPinia } from "pinia";
import { useThemeStore } from "../../src/stores/useThemeStore";

describe("useThemeStore", () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });

  it("switchTheme - should switch the theme to dark", () => {
    const themeStore = useThemeStore();
    themeStore.theme = "light";

    themeStore.switchTheme();

    expect(themeStore.theme).toEqual("dark");
  });

  it("switchTheme - should switch the theme to light", () => {
    const themeStore = useThemeStore();
    themeStore.theme = "dark";

    themeStore.switchTheme();

    expect(themeStore.theme).toEqual("light");
  });

  it("switchStorageTheme - should set the localStorage theme to dark", () => {
    const themeStore = useThemeStore();

    themeStore.switchStorageTheme("dark");

    expect(localStorage.getItem("theme")).toEqual("dark");
  });

  it("switchStorageTheme - should set the localStorage theme to light", () => {
    const themeStore = useThemeStore();

    themeStore.switchStorageTheme("light");

    expect(localStorage.getItem("theme")).toEqual("light");
  });
});
