import { beforeEach, expect, it, describe } from "vitest";
import { setActivePinia, createPinia } from "pinia";
import { useUserStore } from "../../src/stores/useUserStore";

describe("useUserStore", () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });

  describe("actions", () => {
    describe("removeUser", () => {
      it("should remove the user and the user from localStorage", () => {
        const userStore = useUserStore();

        localStorage.setItem(
          "user",
          `{"token":true,"role":"CLIENT","name":"Nawfel","lastname":"Senoussi","email":"nawfelsenoussi@gmail.com"}`,
        );

        userStore.setUser({
          token: "myToken",
          role: "CLIENT",
          name: "Nawfel",
          lastname: "Senoussi",
          email: "nawfelsenoussi@gmail.com",
        });

        userStore.removeUser();

        expect(userStore.getUser).toBeNull();
        expect(localStorage.getItem("user")).toEqual("null");
      });
    });

    describe("setUser", () => {
      it("should set the user and the user in localStorage", () => {
        const userStore = useUserStore();

        userStore.setUser({
          token: "myToken",
          role: "CLIENT",
          name: "Nawfel",
          lastname: "Senoussi",
          email: "nawfelsenoussi@gmail.com",
        });

        expect(userStore.getUser).toEqual({
          token: "myToken",
          role: "CLIENT",
          name: "Nawfel",
          lastname: "Senoussi",
          email: "nawfelsenoussi@gmail.com",
        });

        expect(localStorage.getItem("user")).toEqual(
          `{"token":"myToken","role":"CLIENT","name":"Nawfel","lastname":"Senoussi","email":"nawfelsenoussi@gmail.com"}`,
        );
      });
    });
  });

  describe("getters", () => {
    describe("getRole", () => {
      it("should return the role value", () => {
        const userStore = useUserStore();

        userStore.setUser({
          token: "myToken",
          role: "CLIENT",
          name: "Nawfel",
          lastname: "Senoussi",
          email: "nawfelsenoussi@gmail.com",
          expirationDate: new Date(),
        });

        expect(userStore.getRole).toEqual("CLIENT");
      });
    });

    describe("getToken", () => {
      it("should return the token value", () => {
        const userStore = useUserStore();

        userStore.setUser({
          token: "myToken",
          role: "CLIENT",
          name: "Nawfel",
          lastname: "Senoussi",
          email: "nawfelsenoussi@gmail.com",
        });

        expect(userStore.getToken).toEqual("myToken");
      });
    });

    describe("getLastname", () => {
      it("should return the lastname value", () => {
        const userStore = useUserStore();

        userStore.setUser({
          token: "myToken",
          role: "CLIENT",
          name: "Nawfel",
          lastname: "Senoussi",
          email: "nawfelsenoussi@gmail.com",
        });

        expect(userStore.getLastname).toEqual("Senoussi");
      });
    });

    describe("getName", () => {
      it("should return the name value", () => {
        const userStore = useUserStore();

        userStore.setUser({
          token: "myToken",
          role: "CLIENT",
          name: "Nawfel",
          lastname: "Senoussi",
          email: "nawfelsenoussi@gmail.com",
          expirationDate: new Date(),
        });

        expect(userStore.getName).toEqual("Nawfel");
      });
    });

    describe("getEmail", () => {
      it("should return the email value", () => {
        const userStore = useUserStore();

        userStore.setUser({
          token: "myToken",
          role: "CLIENT",
          name: "Nawfel",
          lastname: "Senoussi",
          email: "nawfelsenoussi@gmail.com",
          expirationDate: new Date(),
        });

        expect(userStore.getEmail).toEqual("nawfelsenoussi@gmail.com");
      });
    });

    describe("getFullName", () => {
      it("should return the full name", () => {
        const userStore = useUserStore();

        userStore.setUser({
          token: "myToken",
          role: "CLIENT",
          name: "Nawfel",
          lastname: "Senoussi",
          email: "nawfelsenoussi@gmail.com",
          expirationDate: new Date(),
        });

        expect(userStore.getFullName).toEqual("Nawfel Senoussi");
      });
    });

    describe("getExpirationDate", () => {
      it("should return the expiration date", () => {
        const userStore = useUserStore();

        userStore.setUser({
          token: "myToken",
          role: "CLIENT",
          name: "Nawfel",
          lastname: "Senoussi",
          email: "nawfelsenoussi@gmail.com",
          expirationDate: "10-02-2024",
        });

        expect(userStore.getExpirationDate).toEqual("10-02-2024");
      });
    });

    describe("isAuthenticated", () => {
      it("should return if the user is authenticated (true case)", () => {
        const userStore = useUserStore();

        userStore.setUser({
          token: "myToken",
          role: "CLIENT",
          name: "Nawfel",
          lastname: "Senoussi",
          email: "nawfelsenoussi@gmail.com",
        });

        expect(userStore.isAuthenticated).toBe(true);
      });

      it("should return if the user is authenticated (false case)", () => {
        const userStore = useUserStore();

        userStore.setUser(null);

        expect(userStore.isAuthenticated).toBe(false);
      });
    });
  });
});
