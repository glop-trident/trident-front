import ErrorView from "../views/../../src/views/ErrorView.vue";

import { expect, it, describe, vi } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";
import { pushSpy } from "../../__mocks__/vue-router";

vi.mock("vue-router");

describe("ErrorView", () => {
  let wrapper: VueWrapper;

  describe("when ErrorView is mounted", () => {
    it("should contain 404 text", () => {
      wrapper = mount(ErrorView);

      expect(wrapper.text()).toContain("404");
    });

    describe("when the redirection button is clicked", () => {
      it("should push to /home route", async () => {
        wrapper = mount(ErrorView);

        const button = wrapper.find("button");
        await button.trigger("click");

        expect(pushSpy).toBeCalled();
        expect(pushSpy).toBeCalledWith("/home");
      });
    });
  });
});
