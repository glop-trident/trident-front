import { todayDate, formatDate } from "../../src/utils/dateUtils";
import { expect, it, describe } from "vitest";

describe("dateUtils", () => {
  describe("todayDate", () => {
    it("should return the current date", () => {
      const today = new Date().toISOString().split("T")[0];
      expect(todayDate).toBe(today);
    });
  });

  describe("formatDate", () => {
    it("should format date correctly", () => {
      const inputDate = new Date("2023-10-11T12:34:56.789Z");
      const formattedDate = formatDate(inputDate);
      expect(formattedDate).toBe("2023-10-11");
    });

    it("should handle different date formats", () => {
      const inputDate = new Date("2022-01-01");
      const formattedDate = formatDate(inputDate);
      expect(formattedDate).toBe("2022-01-01");
    });
  });
});
