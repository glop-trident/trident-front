import { setCurrentTheme } from "../../src/utils/themeUtils";
import { describe, vi, beforeEach, it, expect } from "vitest";

const localStorageMock = {
  getItem: vi.fn(),
};

vi.stubGlobal("localStorage", localStorageMock);

let matches = true;

Object.defineProperty(window, "matchMedia", {
  value: vi.fn(() => ({ matches: matches })),
});

describe("setCurrentTheme", () => {
  beforeEach(() => {
    document.documentElement.classList.remove("dark");
    localStorageMock.getItem.mockClear();
  });

  it("should set dark theme if localStorage theme is dark", () => {
    localStorageMock.getItem.mockReturnValueOnce("dark");
    matches = true;

    setCurrentTheme();

    expect(document.documentElement.classList.contains("dark")).toBe(true);
  });

  it("should set dark theme if prefers-color-scheme is dark", () => {
    localStorageMock.getItem.mockReturnValueOnce(null);
    matches = true;

    setCurrentTheme();

    expect(document.documentElement.classList.contains("dark")).toBe(true);
  });

  it("should not set dark theme if neither localStorage nor prefers-color-scheme is dark", () => {
    localStorageMock.getItem.mockReturnValueOnce("light");
    matches = false;

    setCurrentTheme();

    expect(document.documentElement.classList.contains("dark")).toBe(false);
  });
});
