import {
  phonePattern,
  frenchZipcodePattern,
} from "../../src/utils/patternUtils";
import { expect, it, describe } from "vitest";

describe("patternUtils", () => {
  describe("phonePattern", () => {
    it("should be valid", () => {
      const phoneNumber = "0686062220";
      const isValid = new RegExp(phonePattern).test(phoneNumber);
      expect(isValid).toBe(true);
    });

    it("should be invalid", () => {
      const phoneNumber = "01234567891";
      const isValid = new RegExp(phonePattern).test(phoneNumber);
      expect(isValid).toBe(false);
    });
  });

  describe("frenchZipcodePattern", () => {
    it("should be valid", () => {
      const frenchPostalCode = "59100";
      const isValid = new RegExp(frenchZipcodePattern).test(frenchPostalCode);
      expect(isValid).toBe(true);
    });

    it("should be invalid", () => {
      const frenchPostalCode = "5500010";
      const isValid = new RegExp(frenchZipcodePattern).test(frenchPostalCode);
      expect(isValid).toBe(false);
    });
  });
});
