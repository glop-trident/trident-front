import ButtonTransparent from "../../../../src/components/ui/button/ButtonTransparent.vue";

import { expect, it, describe } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";

describe("ButtonTransparent", () => {
  let wrapper: VueWrapper;

  describe("when the button is mounted", () => {
    it("should render the button with the label", () => {
      wrapper = mount(ButtonTransparent, {
        props: {
          color: "primary",
          label: "Accept",
        },
      });

      const button = wrapper.find("button");

      expect(button.exists()).toBe(true);
      expect(button.text()).toContain("Accept");
    });

    describe("when color is provided", () => {
      describe("when color is primary", () => {
        it("should add primary button class", () => {
          wrapper = mount(ButtonTransparent, {
            props: {
              color: "primary",
              label: "Accept",
            },
          });

          const button = wrapper.find("button");

          expect(button.classes()).toContain("hover:bg-primary-500");
          expect(button.classes()).toContain("dark:hover:bg-primary-300");
          expect(button.classes()).toContain("switch-text-brand");
        });
      });

      describe("when color is red", () => {
        it("should add red button class", () => {
          wrapper = mount(ButtonTransparent, {
            props: {
              color: "red",
              label: "Accept",
            },
          });

          const button = wrapper.find("button");

          expect(button.classes()).toContain("hover:bg-error-500");
          expect(button.classes()).toContain("dark:hover:bg-error-400");
          expect(button.classes()).toContain("text-error-500");
          expect(button.classes()).toContain("dark:text-error-400");
        });
      });
    });

    describe("when isSquare is provided", () => {
      describe("when isSquare is false", () => {
        it("should add square class", () => {
          wrapper = mount(ButtonTransparent, {
            props: {
              color: "primary",
              label: "Accept",
              isSquare: false,
            },
          });

          const button = wrapper.find("button");

          expect(button.classes()).toContain("px-4");
          expect(button.classes()).toContain("py-2");
        });
      });

      describe("when isSquare is true", () => {
        it("should add square class", () => {
          wrapper = mount(ButtonTransparent, {
            props: {
              color: "primary",
              label: "Accept",
              isSquare: true,
            },
          });

          const button = wrapper.find("button");

          expect(button.classes()).toContain("px-3");
          expect(button.classes()).toContain("py-2");
        });
      });
    });

    describe("when the user click on the button", () => {
      describe("when the button is enabled", () => {
        it("should emit click", async () => {
          const wrapper = mount(ButtonTransparent, {
            props: {
              color: "primary",
              label: "Click me!",
            },
          });

          const button = wrapper.find("button");

          await button.trigger("click");

          expect(wrapper.emitted("click")).toBeTruthy();
        });
      });

      describe("when the button is disabled", () => {
        it("should not emit click", async () => {
          const wrapper = mount(ButtonTransparent, {
            props: {
              color: "primary",
              label: "Click me!",
              disabled: true,
            },
          });

          const button = wrapper.find("button");

          await button.trigger("click");

          expect(wrapper.emitted("click")).toBeFalsy();
        });
      });
    });
  });
});
