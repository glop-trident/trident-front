///////// IMPORT COMPONENTS
import ToggleSwitch from "../../../src/components/ui/ToggleSwitch.vue";

///////// IMPORT EXTERNAL LIBS
import { expect, it, describe } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";

describe("ToggleSwitch", () => {
  let wrapper: VueWrapper;

  describe("when ToggleSwitch is mounted", () => {
    describe("when label is provided", () => {
      it("should render the label text", () => {
        wrapper = mount(ToggleSwitch, {
          props: {
            modelValue: true,
            disabled: false,
            label: "The label",
          },
        });

        const label = wrapper.find("label");

        expect(label.text()).toEqual("The label");
      });
    });

    describe("when modelValue is true", () => {
      it("should render the input and have true for value attributes", () => {
        wrapper = mount(ToggleSwitch, {
          props: {
            modelValue: true,
            disabled: false,
          },
        });

        const input = wrapper.find("input[type=checkbox]");

        expect(input.exists()).toBe(true);
        expect(input.attributes("value")).toEqual("true");
      });
    });

    describe("when modelValue is false", () => {
      it("should render the input and have false for value attributes", () => {
        wrapper = mount(ToggleSwitch, {
          props: {
            modelValue: false,
            disabled: false,
          },
        });

        const input = wrapper.find("input[type=checkbox]");

        expect(input.exists()).toBe(true);
        expect(input.attributes("value")).toEqual("false");
      });
    });

    describe("when the user change the checkbox", () => {
      describe("when the checkbox is enabled", () => {
        it("should emit update:modalValue with the new value", async () => {
          wrapper = mount(ToggleSwitch, {
            props: {
              modelValue: false,
            },
          });

          const input = wrapper.find("input[type=checkbox]");

          await input.setValue(true);

          expect(wrapper.emitted("update:modelValue")).toBeTruthy();
          expect(wrapper.emitted("update:modelValue")).toEqual([[true]]);
        });
      });

      describe("when the checkbox is disabled", () => {
        it("should not emit update:modelValue", async () => {
          wrapper = mount(ToggleSwitch, {
            props: {
              modelValue: false,
              disabled: true,
            },
          });

          const input = wrapper.find("input[type=checkbox]");

          await input.setValue(true);

          expect(wrapper.emitted("update:modelValue")).toBeFalsy();
        });
      });
    });
  });
});
