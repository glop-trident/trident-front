import MainInput from "../../../src/components/ui/MainInput.vue";

import { expect, it, describe } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";

describe("MainInput", () => {
  let wrapper: VueWrapper;

  describe("when MainInput is mounted", () => {
    it("should render the input with with the correct type and modelValue", () => {
      wrapper = mount(MainInput, {
        props: {
          modelValue: "John",
          type: "text",
          name: "test",
        },
      });

      const input = wrapper.find("input");

      expect(input.exists()).toBe(true);
      expect(input.attributes("type")).toEqual("text");
      expect(input.element.value).toEqual("John");
    });

    describe("when the label is provided", () => {
      it("should render the label", () => {
        wrapper = mount(MainInput, {
          props: {
            modelValue: "",
            type: "text",
            name: "test",
            label: "Name",
          },
        });

        const label = wrapper.find("label");

        expect(label.exists()).toBe(true);
        expect(label.text()).toEqual("Name");
      });
    });

    describe("when the placeholder is provided", () => {
      it("should add placeholder attributes to the input", () => {
        wrapper = mount(MainInput, {
          props: {
            modelValue: "",
            type: "text",
            name: "test",
            placeholder: "John",
          },
        });

        const input = wrapper.find("input");

        expect(input.attributes("placeholder")).toEqual("John");
      });
    });

    describe("when the input is disabled", () => {
      it("should add disabled attribute to the input", async () => {
        wrapper = mount(MainInput, {
          props: {
            modelValue: "",
            type: "text",
            name: "test",
            disabled: true,
          },
        });

        const input = wrapper.find("input");

        expect(input.attributes()).toHaveProperty("disabled");
      });
    });

    describe("when the input is set", () => {
      it("should emit 'update:modelValue' with the new value", async () => {
        wrapper = mount(MainInput, {
          props: {
            modelValue: "Old value",
            type: "text",
            name: "test",
          },
        });

        const input = wrapper.find("input");

        await input.setValue("New value");

        expect(wrapper.emitted()).toHaveProperty("update:modelValue");
        expect(wrapper.emitted("update:modelValue")).toEqual([["New value"]]);
      });
    });

    describe("when the input is email", () => {
      describe("when the input is required", () => {
        describe("when modelValue is empty", () => {
          it("should display 'Champ requis' error message", () => {
            wrapper = mount(MainInput, {
              props: {
                modelValue: "",
                type: "text",
                name: "test",
                required: true,
                errorMessage: "Please enter a valid e-mail address",
              },
            });

            const errorMessage = wrapper.find("p");

            expect(errorMessage.text()).toEqual("Champ requis");
          });
        });

        describe("when modelValue is invalid", () => {
          it("should display the error message", () => {
            wrapper = mount(MainInput, {
              props: {
                modelValue: "John",
                type: "text",
                name: "test",
                required: true,
                errorMessage: "Please enter a valid e-mail address",
              },
            });

            const errorMessage = wrapper.find("p");

            expect(errorMessage.text()).toEqual(
              "Please enter a valid e-mail address"
            );
          });
        });
      });
    });

    describe("when the input is invalid", () => {
      describe("when the input is not blur", () => {
        it("should not change the border color of the input", () => {
          wrapper = mount(MainInput, {
            props: {
              modelValue: "",
              type: "text",
              name: "test",
              required: true,
            },
          });

          const input = wrapper.find("input");

          expect(input.classes()).not.toContain("switch-border-input-error");
        });
      });

      describe("when the input is blur", () => {
        it("should change the border color of the input", async () => {
          wrapper = mount(MainInput, {
            props: {
              modelValue: "",
              type: "text",
              name: "test",
              required: true,
            },
          });

          const input = wrapper.find("input");

          await input.trigger("blur");

          expect(input.classes()).toContain("switch-border-input-error");
        });
      });
    });
  });
});
