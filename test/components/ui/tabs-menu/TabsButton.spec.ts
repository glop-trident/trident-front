import TabsButton from "../../../../src/components/ui/tabs-menu/TabsButton.vue";

import { expect, it, describe } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";

describe("TabsButton", () => {
  let wrapper: VueWrapper;

  describe("when the tabs button is mounted", () => {
    it("should render the button with the label and icon", () => {
      wrapper = mount(TabsButton, {
        props: {
          active: false,
          label: "General",
          icon: "fa-solid fa-home",
        },
      });

      const button = wrapper.find("button");

      expect(button.exists()).toBe(true);
      expect(button.text()).toContain("General");

      expect(button.find("i").exists()).toBe(true);
      expect(button.find("i").classes()).toContain("fa-solid");
      expect(button.find("i").classes()).toContain("fa-home");
    });

    describe("when the active is false", () => {
      it("should not contain active class", () => {
        wrapper = mount(TabsButton, {
          props: {
            active: false,
            label: "General",
            icon: "fa-solid fa-home",
          },
        });

        const button = wrapper.find("button");

        expect(button.classes()).not.toContain("border-primary-500");
      });
    });

    describe("when the active is true", () => {
      it("should contain active class", () => {
        wrapper = mount(TabsButton, {
          props: {
            active: true,
            label: "General",
            icon: "fa-solid fa-home",
          },
        });

        const button = wrapper.find("button");

        expect(button.classes()).toContain("border-primary-500");
      });
    });
  });
});
