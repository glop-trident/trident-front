import TabsMenu from "../../../../src/components/ui/tabs-menu/TabsMenu.vue";

import { expect, it, describe } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";

describe("TabsMenu", () => {
  let wrapper: VueWrapper;

  describe("when the tabs menu is mounted", () => {
    it("should render 2 button with their label", () => {
      wrapper = mount(TabsMenu, {
        props: {
          activeIndex: 1,
          tabHeaders: [
            { id: "1", label: "General", icon: "fa-solid fa-home" },
            { id: "2", label: "Password", icon: "fa-solid fa-home" },
          ],
        },
      });

      const [generalButton, passwordButton] = wrapper.findAll("button");

      expect(generalButton.exists()).toBe(true);
      expect(generalButton.text()).toContain("General");

      expect(passwordButton.exists()).toBe(true);
      expect(passwordButton.text()).toContain("Password");
    });

    describe("when slots are provided", () => {
      it("should render the correct slot", async () => {
        wrapper = mount(TabsMenu, {
          props: {
            tabHeaders: [
              { id: "1", label: "General", icon: "fa-solid fa-home" },
              { id: "2", label: "Password", icon: "fa-solid fa-home" },
            ],
          },
          slots: {
            item1: "Item 1",
            item2: "Item 2",
          },
        });

        const [generalButton, passwordButton] = wrapper.findAll("button");

        await generalButton.trigger("click");

        expect(wrapper.text()).toContain("Item 1");

        await passwordButton.trigger("click");

        expect(wrapper.text()).toContain("Item 2");
      });
    });
  });
});
