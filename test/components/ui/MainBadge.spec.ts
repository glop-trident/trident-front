import MainBadge from "@/components/ui/MainBadge.vue";

import { expect, it, describe } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";

describe("MainBadge", () => {
  let wrapper: VueWrapper;

  describe("when the badge is mounted", () => {
    it("should render the badge and the label in the badge", () => {
      wrapper = mount(MainBadge, {
        props: {
          variant: "soft",
          color: "blue",
          label: "Accept",
          radius: "sm",
        },
      });

      const badge = wrapper.find('[role="status"]');

      expect(badge.exists()).toBe(true);
      expect(badge.text()).toContain("Accept");
    });

    describe("when variant is provided", () => {
      describe("when variant is solid", () => {
        it("should add 'badge-solid' class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "blue",
              label: "Accept",
              radius: "sm",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("badge-solid");
        });
      });

      describe("when variant is soft", () => {
        it("should add 'badge-soft' class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "soft",
              color: "blue",
              label: "Accept",
              radius: "sm",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("badge-soft");
        });
      });

      describe("when variant is outline", () => {
        it("should add 'badge-outline' class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "outline",
              color: "blue",
              label: "Accept",
              radius: "sm",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("badge-outline");
        });
      });
    });

    describe("when color is provided", () => {
      describe("when color is blue", () => {
        it("should add 'badge-blue' class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "blue",
              label: "Accept",
              radius: "sm",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("badge-blue");
        });
      });

      describe("when color is red", () => {
        it("should add 'badge-red' class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "red",
              label: "Accept",
              radius: "sm",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("badge-red");
        });
      });

      describe("when color is yellow", () => {
        it("should add 'badge-yellow' class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "yellow",
              label: "Accept",
              radius: "sm",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("badge-yellow");
        });
      });

      describe("when color is orange", () => {
        it("should add 'badge-orange' class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "orange",
              label: "Accept",
              radius: "sm",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("badge-orange");
        });
      });
    });

    describe("when size is provided", () => {
      describe("when size is tiny", () => {
        it("should add the tiny size class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "orange",
              label: "Accept",
              size: "tiny",
              radius: "sm",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("px-2");
          expect(badge.classes()).toContain("py-0.5");
        });
      });

      describe("when size is small", () => {
        it("should add the small size class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "orange",
              label: "Accept",
              size: "small",
              radius: "sm",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("px-3");
          expect(badge.classes()).toContain("py-1");
        });
      });

      describe("when size is medium", () => {
        it("should add the medium size class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "orange",
              label: "Accept",
              size: "medium",
              radius: "sm",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("px-4");
          expect(badge.classes()).toContain("py-2");
        });
      });

      describe("when size is large", () => {
        it("should add the large size class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "orange",
              label: "Accept",
              size: "large",
              radius: "sm",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("px-5");
          expect(badge.classes()).toContain("py-3");
        });
      });
    });

    describe("when radius is provided", () => {
      describe("when radius is none", () => {
        it("should add the 'rounded-none' class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "orange",
              label: "Accept",
              size: "large",
              radius: "none",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("rounded-none");
        });
      });

      describe("when radius is sm", () => {
        it("should add the 'rounded-sm' class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "orange",
              label: "Accept",
              size: "large",
              radius: "sm",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("rounded-sm");
        });
      });

      describe("when radius is md", () => {
        it("should add the 'rounded-md' class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "orange",
              label: "Accept",
              size: "large",
              radius: "md",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("rounded-md");
        });
      });

      describe("when radius is lg", () => {
        it("should add the 'rounded-lg' class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "orange",
              label: "Accept",
              size: "large",
              radius: "lg",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("rounded-lg");
        });
      });

      describe("when radius is full", () => {
        it("should add the 'rounded-full' class", () => {
          wrapper = mount(MainBadge, {
            props: {
              variant: "solid",
              color: "orange",
              label: "Accept",
              size: "large",
              radius: "full",
            },
          });

          const badge = wrapper.find('[role="status"]');

          expect(badge.classes()).toContain("rounded-full");
        });
      });
    });
  });
});
