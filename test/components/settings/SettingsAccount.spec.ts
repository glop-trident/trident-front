import SettingsAccount from "../../../src/components/settings/SettingsAccount.vue";

import { expect, it, describe, vi } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";
import { reactive, ref } from "vue";

vi.mock("../../../src/components/settings/composables/useSettings", () => {
  return {
    useSettings: () => {
      const user = reactive({
        email: "",
        role: "CLIENT",
        lastName: "",
        name: "",
        birthday: new Date(),
        city: "",
        phoneNumber: "",
        zipCode: "",
        image: "",
      });

      const password = ref("");

      const creditCard = reactive({
        owner: "",
        cardNumber: "",
        expirationDate: new Date(),
        crypto: "",
      });

      return {
        user,
        creditCard,
        password,
      };
    },
  };
});

describe("SettingsAccount", () => {
  let wrapper: VueWrapper;

  describe("when SettingsAccount is mounted", () => {
    it("should render three button on settings tabs", () => {
      wrapper = mount(SettingsAccount);

      const [generalButton, passwordButton, creditCardButton] =
        wrapper.findAll("button");

      expect(generalButton.exists()).toBe(true);
      expect(generalButton.text()).toContain("Géneral");

      expect(passwordButton.exists()).toBe(true);
      expect(passwordButton.text()).toContain("Mot de passe");

      expect(creditCardButton.exists()).toBe(true);
      expect(creditCardButton.text()).toContain("Carte bancaire");
    });

    describe("when generalButton is trigger", () => {
      it("should render user icon and input", async () => {
        wrapper = mount(SettingsAccount);

        const [generalButton] = wrapper.findAll("button");

        await generalButton.trigger("click");

        const [
          emailInput,
          roleInput,
          nameInput,
          lastnameInput,
          phoneInput,
          cityInput,
          zipcodeInput,
          birthdayInput,
        ] = wrapper.findAll("input");

        expect(emailInput.attributes("name")).toBe("email");
        expect(roleInput.attributes("name")).toBe("role");
        expect(nameInput.attributes("name")).toBe("name");
        expect(lastnameInput.attributes("name")).toBe("lastname");
        expect(phoneInput.attributes("name")).toBe("phone");
        expect(cityInput.attributes("name")).toBe("city");
        expect(zipcodeInput.attributes("name")).toBe("zipcode");
        expect(birthdayInput.attributes("name")).toBe("birth");
      });
    });

    describe("when passwordButton is trigger", () => {
      it("should render password icon and input", async () => {
        wrapper = mount(SettingsAccount);

        const [, passwordButton] = wrapper.findAll("button");

        await passwordButton.trigger("click");

        const [passwordInput, confirmPasswordInput] = wrapper.findAll("input");

        expect(passwordInput.attributes("name")).toBe("password");
        expect(confirmPasswordInput.attributes("name")).toBe("confirmPassword");
      });
    });

    describe("when creditCardButton is trigger", () => {
      it("should render credit card icon and input", async () => {
        wrapper = mount(SettingsAccount);

        const [, , creditCardButton] = wrapper.findAll("button");

        await creditCardButton.trigger("click");

        const [ownerInput, cardNumberInput, cryptoInput, expirationDateInput] =
          wrapper.findAll("input");

        expect(ownerInput.attributes("name")).toBe("owner");
        expect(cardNumberInput.attributes("name")).toBe("cardNumber");
        expect(cryptoInput.attributes("name")).toBe("crypto");
        expect(expirationDateInput.attributes("name")).toBe("expirationDate");
      });
    });
  });
});
