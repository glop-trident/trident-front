import CreditCardFormSettings from "../../../src/components/settings/CreditCardFormSettings.vue";

import { expect, it, describe, vi } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";
import { reactive } from "vue";

vi.mock("../../../src/components/settings/composables/useSettings", () => {
  return {
    useSettings: () => {
      const user = reactive({
        email: "",
        role: "CLIENT",
        lastName: "",
        name: "",
        birthday: new Date(),
        city: "",
        phoneNumber: "",
        zipCode: "",
        image: "",
        password: "",
      });

      const creditCard = reactive({
        owner: "",
        cardNumber: "",
        expirationDate: new Date(),
        crypto: "",
      });

      return {
        user,
        creditCard,
      };
    },
  };
});

describe("CreditCardFormSettings", () => {
  let wrapper: VueWrapper;

  describe("when CreditCardFormSettings is mounted", () => {
    it("should render credit card input", () => {
      wrapper = mount(CreditCardFormSettings);

      const [ownerInput, cardNumbernput, cryptoInput, expirationDateInput] =
        wrapper.findAll("input");

      expect(ownerInput.attributes("type")).toBe("text");
      expect(cardNumbernput.attributes("type")).toBe("text");
      expect(cryptoInput.attributes("type")).toBe("text");
      expect(expirationDateInput.attributes("type")).toBe("date");

      expect(ownerInput.attributes("name")).toBe("owner");
      expect(cardNumbernput.attributes("name")).toBe("cardNumber");
      expect(cryptoInput.attributes("name")).toBe("crypto");
      expect(expirationDateInput.attributes("name")).toBe("expirationDate");
    });
  });
});
