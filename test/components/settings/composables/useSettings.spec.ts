import { describe, expect, it, vi } from "vitest";
import { mount } from "@vue/test-utils";
import { useSettings } from "../../../../src/components/settings/composables/useSettings";
import { createTestingPinia } from "@pinia/testing";
import axiosInstance from "../../../../src/services/instance/axiosWithToken";
import { Axios } from "axios";

vi.mock("../../../src/services/instance/axiosWithToken", async () => {
  const axios: Axios = await vi.importActual(
    "../../../src/services/instance/axiosWithToken",
  );

  return { ...axios, get: vi.fn() };
});

describe.skip("useSettings", () => {
  it("should load user settings correctly", async () => {
    axiosInstance.get = vi.fn().mockResolvedValueOnce({
      data: {
        email: "test@example.com",
        role: "user",
        lastName: "Doe",
        name: "John",
        birthday: "1990-01-01",
        city: "New York",
        phoneNumber: "1234567890",
        zipCode: "10001",
        image: "avatar.jpg",
      },
    });

    localStorage.setItem(
      "user",
      `{"token":true,"role":"CLIENT","name":"Nawfel","lastname":"Senoussi","email":"nawfelsenoussi@gmail.com"}`,
    );

    mount({
      global: {
        plugins: [createTestingPinia()],
      },
      template: "<div></div>",
      setup() {
        const { user, loadSettings } = useSettings();

        loadSettings();

        return { user };
      },
    });

    const { user } = useSettings();

    expect(user.email).toBe("test@example.com");
    expect(user.role).toBe("user");
    expect(user.lastName).toBe("Doe");
    expect(user.name).toBe("John");
    expect(user.birthday).toBe("1990-01-01");
    expect(user.city).toBe("New York");
    expect(user.phoneNumber).toBe("1234567890");
    expect(user.zipCode).toBe("10001");
    expect(user.image).toBe("avatar.jpg");
  });
});
