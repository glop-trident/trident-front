import UserFormSettings from "../../../src/components/settings/UserFormSettings.vue";

import { expect, it, describe, vi } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";
import { reactive } from "vue";

vi.mock("../../../src/components/settings/composables/useSettings", () => {
  return {
    useSettings: () => {
      const user = reactive({
        email: "",
        role: "CLIENT",
        lastName: "",
        name: "",
        birthday: new Date(),
        city: "",
        phoneNumber: "",
        zipCode: "",
        image: "",
        password: "",
      });

      const creditCard = reactive({
        owner: "",
        cardNumber: "",
        expirationDate: new Date(),
        crypto: "",
      });

      return {
        user,
        creditCard,
      };
    },
  };
});

describe("UserFormSettings", () => {
  let wrapper: VueWrapper;

  describe("when UserFormSettings is mounted", () => {
    it("should render user input", () => {
      wrapper = mount(UserFormSettings);

      const [
        emailInput,
        roleInput,
        nameInput,
        lastnameInput,
        phoneInput,
        cityInput,
        zipcodeInput,
        birthdayInput,
      ] = wrapper.findAll("input");

      expect(emailInput.attributes("type")).toBe("email");
      expect(roleInput.attributes("type")).toBe("text");
      expect(nameInput.attributes("type")).toBe("text");
      expect(lastnameInput.attributes("type")).toBe("text");
      expect(phoneInput.attributes("type")).toBe("tel");
      expect(cityInput.attributes("type")).toBe("text");
      expect(zipcodeInput.attributes("type")).toBe("text");
      expect(birthdayInput.attributes("type")).toBe("date");

      expect(emailInput.attributes("name")).toBe("email");
      expect(roleInput.attributes("name")).toBe("role");
      expect(nameInput.attributes("name")).toBe("name");
      expect(lastnameInput.attributes("name")).toBe("lastname");
      expect(phoneInput.attributes("name")).toBe("phone");
      expect(cityInput.attributes("name")).toBe("city");
      expect(zipcodeInput.attributes("name")).toBe("zipcode");
      expect(birthdayInput.attributes("name")).toBe("birth");

      expect(emailInput.element.disabled).toBe(true);
      expect(roleInput.element.disabled).toBe(true);
    });
  });
});
