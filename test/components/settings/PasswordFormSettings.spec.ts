import PasswordFormSettings from "../../../src/components/settings/PasswordFormSettings.vue";

import { expect, it, describe, vi } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";
import { reactive, ref } from "vue";

vi.mock("../../../src/components/settings/composables/useSettings", () => {
  return {
    useSettings: () => {
      const user = reactive({
        email: "",
        role: "CLIENT",
        lastName: "",
        name: "",
        birthday: new Date(),
        city: "",
        phoneNumber: "",
        zipCode: "",
        image: "",
        password: "",
      });

      const password = ref("");

      const creditCard = reactive({
        owner: "",
        cardNumber: "",
        expirationDate: new Date(),
        crypto: "",
      });

      return {
        user,
        creditCard,
        password,
      };
    },
  };
});

describe("PasswordFormSettings", () => {
  let wrapper: VueWrapper;

  describe("when PasswordFormSettings is mounted", () => {
    it("should render user input", () => {
      wrapper = mount(PasswordFormSettings);

      const [passwordInput, confirmPasswordInput] = wrapper.findAll("input");

      expect(passwordInput.attributes("type")).toBe("password");
      expect(confirmPasswordInput.attributes("type")).toBe("password");

      expect(passwordInput.attributes("name")).toBe("password");
      expect(confirmPasswordInput.attributes("name")).toBe("confirmPassword");
    });
  });
});
