import BaseLayout from "../../../src/components/layout/BaseLayout.vue";

import { expect, it, describe, vi } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";

vi.mock("vue-router");

describe("BaseLayout", () => {
  let wrapper: VueWrapper;

  describe("when the base layout is mounted", () => {
    describe("when the topbar slot is provided", () => {
      it("should render the topbar slot", () => {
        wrapper = mount(BaseLayout, {
          slots: {
            topbar: "<span> Topbar slot </span>",
          },
        });

        expect(wrapper.text()).toContain("Topbar slot");
      });
    });

    describe("when the main-content slot is provided", () => {
      it("should render the main content slot", () => {
        wrapper = mount(BaseLayout, {
          slots: {
            "main-content": "<span> Main content slot </span>",
          },
        });

        expect(wrapper.text()).toContain("Main content slot");
      });
    });
  });
});
