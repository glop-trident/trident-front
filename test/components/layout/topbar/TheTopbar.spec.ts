import TheTopbar from "../../../../src/components/layout/topbar/TheTopbar.vue";

import { expect, it, describe, vi } from "vitest";
import { RouterLinkStub, VueWrapper, mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";
import { useThemeStore } from "../../../../src/stores/useThemeStore";

vi.mock("vue-router");

Object.defineProperty(window, "matchMedia", {
  value: vi.fn().mockImplementation((query) => ({
    matches: false,
    media: query,
  })),
});

function mountTheTopbar() {
  return mount(TheTopbar, {
    global: {
      plugins: [createTestingPinia()],
      stubs: {
        RouterLink: RouterLinkStub,
      },
      directives: {
        "click-away": vi.fn(),
      },
    },
  });
}

describe("TheTopbar", () => {
  let wrapper: VueWrapper;

  describe("when TheTopbar is mounted", () => {
    it("should render the title", () => {
      wrapper = mountTheTopbar();

      expect(wrapper.find("h1").text()).toEqual("ShopLoc");
    });

    it("should render the top link", () => {
      wrapper = mountTheTopbar();

      const [homeLink, storeLink] = wrapper.findAll("a");

      expect(homeLink.text()).toContain("Accueil");
      expect(storeLink.text()).toContain("Boutique");
    });

    describe("when theme button is clicked", () => {
      it("should call switchTheme from themeStore", async () => {
        wrapper = mountTheTopbar();
        const themeStore = useThemeStore();

        const [themeButton] = wrapper.findAll("button");

        await themeButton.trigger("click");

        expect(themeStore.switchTheme).toBeCalled();
      });
    });

    describe("when dropdown button is clicked", () => {
      it("should render the dropdown link", async () => {
        wrapper = mountTheTopbar();

        const [, avatarButton] = wrapper.findAll("button");

        // OPEN THE DROPDOWN MENY
        await avatarButton.trigger("click");

        const dropdown = wrapper.find('[data-testid="avatar-dropdown"]');
        const [settingsLink, disconnectLink] = dropdown.findAll("button");

        expect(settingsLink.text()).toContain("Paramètres");
        expect(disconnectLink.text()).toContain("Déconnexion");
      });
    });
  });
});
