import AvatarDropdown from "../../../../src/components/layout/topbar/AvatarDropdown.vue";

import { expect, it, describe, vi, beforeEach, afterEach } from "vitest";
import { RouterLinkStub, VueWrapper, mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";
import { pushSpy } from "../../../../__mocks__/vue-router";
import { useUserStore } from "../../../../src/stores/useUserStore";

vi.mock("vue-router");

function mountAvatarDropdown() {
  return mount(AvatarDropdown, {
    global: {
      stubs: {
        RouterLink: RouterLinkStub,
      },
      plugins: [createTestingPinia()],
      directives: {
        "click-away": vi.fn(),
      },
    },
  });
}

describe("AvatarDropdown", () => {
  let wrapper: VueWrapper;

  beforeEach(() => {
    const newUser = {
      token: true,
      lastname: "Senoussi",
      name: "Nawfel",
      email: "nawfelsenoussi59@gmail.com",
      role: "CLIENT",
    };
    localStorage.setItem("user", JSON.stringify(newUser));
  });

  afterEach(() => {
    localStorage.removeItem("user");
  });

  describe("when AvatarDropdown is mounted", () => {
    it("should render the initial in the avatar button", () => {
      wrapper = mountAvatarDropdown();

      expect(wrapper.find("button").text()).toContain("NS");
    });

    describe("when the disconnect button is clicked", () => {
      it("should call push the new route and remove the user from the store", async () => {
        wrapper = mountAvatarDropdown();

        const userStore = useUserStore();

        // OPEN THE DROPDOWN MENU
        await wrapper.find("button").trigger("click");

        const dropdown = wrapper.find('[data-testid="avatar-dropdown"]');
        const [, disconnectButton] = dropdown.findAll("button");

        await disconnectButton.trigger("click");

        expect(pushSpy).toBeCalled();
        expect(pushSpy).toBeCalledWith("/login");
        expect(userStore.removeUser).toBeCalled();
      });
    });

    describe("when the avatar button is clicked", () => {
      describe("one time", () => {
        it("should display the dropdown", async () => {
          wrapper = mountAvatarDropdown();

          // OPEN THE DROPDOWN MENU
          await wrapper.find("button").trigger("click");

          const dropdown = wrapper.find('[data-testid="avatar-dropdown"]');
          const [settingsButton, disconnectButton] = dropdown.findAll("button");

          expect(dropdown.exists()).toBe(true);
          expect(settingsButton.exists()).toBe(true);
          expect(settingsButton.text()).toContain("Paramètres");
          expect(disconnectButton.exists()).toBe(true);
          expect(disconnectButton.text()).toContain("Déconnexion");
        });
      });

      describe("two time", () => {
        it("should not display the dropdown", async () => {
          wrapper = mountAvatarDropdown();

          // OPEN THE DROPDOWN MENU
          await wrapper.find("button").trigger("click");

          // CLOSE THE DROPDOWN MENU
          await wrapper.find("button").trigger("click");

          expect(wrapper.find('[data-testid="avatar-dropdown"]').exists()).toBe(
            false,
          );
        });
      });
    });
  });
});
