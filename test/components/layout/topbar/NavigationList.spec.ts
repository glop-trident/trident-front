import NavigationList from "../../../../src/components/layout/topbar/NavigationList.vue";
import { NavigationContent } from "../../../../src/components/layout/topbar/types/NavigationContent";

import { expect, it, describe } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";

function mountNavigationList(navigationList: NavigationContent[]) {
  return mount(NavigationList, {
    global: {
      stubs: {
        RouterLink: {
          template: "<a><slot /></a>",
        },
      },
    },
    props: {
      navigationList: navigationList,
    },
  });
}

describe("NavigationList", () => {
  let wrapper: VueWrapper;

  describe("when the NavigationList is mounted", () => {
    describe("when navigationList props is provided", () => {
      describe("when the navigationList is available", () => {
        it("should render the a tag with the title", () => {
          wrapper = mountNavigationList([
            {
              title: "Home",
              icon: "fa-solid fa-home",
              routePath: "/route/path",
              available: true,
            },
          ]);

          expect(wrapper.find("a").exists()).toBe(true);
          expect(wrapper.find("a").text()).toEqual("Home");
        });

        it("should render the correct 'to' attribute", () => {
          wrapper = mountNavigationList([
            {
              title: "Home",
              icon: "fa-solid fa-home",
              routePath: "/route/path",
              available: true,
            },
          ]);

          expect(wrapper.find("a").attributes("to")).toEqual("/route/path");
        });
      });

      describe("when the navigationList is not available", () => {
        it("should render not render the a tag", () => {
          wrapper = mountNavigationList([
            {
              title: "Home",
              icon: "fa-solid fa-home",
              routePath: "/route/path",
              available: false,
            },
          ]);

          expect(wrapper.find("a").exists()).toBe(false);
        });
      });

      describe("when navigationList contain multiple element", () => {
        it("should render not render the a tag", () => {
          wrapper = mountNavigationList([
            {
              title: "Home",
              icon: "fa-solid fa-home",
              routePath: "/route/home",
              available: true,
            },
            {
              title: "Settings",
              icon: "fa-solid fa-gear",
              routePath: "/route/settings",
              available: true,
            },
          ]);

          const elements = wrapper.findAll("li");

          expect(elements).toHaveLength(2);
        });
      });
    });
  });
});
