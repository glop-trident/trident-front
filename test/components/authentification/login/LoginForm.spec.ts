import LoginForm from "../../../../src/components/authentification/login/LoginForm.vue";

import { expect, it, describe, vi } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";
import { createTestingPinia } from "@pinia/testing";

vi.mock("vue-router");

function mountLoginForm() {
  return mount(LoginForm, {
    global: {
      plugins: [createTestingPinia()],
    },
  });
}

describe("LoginForm", () => {
  let wrapper: VueWrapper;

  describe("when LoginForm is mounted", () => {
    describe("when the form is not fill", () => {
      it("should invalidate the form", async () => {
        wrapper = mountLoginForm();

        const form = wrapper.find("form").element;

        expect(form.checkValidity()).toBe(false);
      });
    });

    describe("when the form is fill", () => {
      it("should validate the form", async () => {
        wrapper = mountLoginForm();

        const [email, password] = wrapper.findAll("input");
        const form = wrapper.find("form").element;

        await email.setValue("lebronjames@gmail.com");
        await password.setValue("test");

        expect(form.checkValidity()).toBe(true);
      });
    });
  });
});
