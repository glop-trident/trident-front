import RegisterForm from "../../../../src/components/authentification/register/RegisterForm.vue";

import { expect, it, describe, vi } from "vitest";
import { VueWrapper, mount } from "@vue/test-utils";

vi.mock("vue-router");

describe("RegisterForm", () => {
  let wrapper: VueWrapper;

  describe("when RegisterForm is mounted", () => {
    describe("when the form is not fill", () => {
      it("should invalidate the form", async () => {
        wrapper = mount(RegisterForm);

        const form = wrapper.find("form").element;

        expect(form.checkValidity()).toBe(false);
      });
    });

    describe.skip("when the form is fill", () => {
      it("should validate the form", async () => {
        wrapper = mount(RegisterForm);

        const form = wrapper.find("form").element;
        const [
          name,
          lastname,
          email,
          phone,
          city,
          zipcode,
          birthDate,
          password,
          confirmPassword,
          marketing,
        ] = wrapper.findAll("input");

        await name.setValue("James");
        await lastname.setValue("Lebron");
        await email.setValue("lebronjames@gmail.com");
        await phone.setValue("0686868686");
        await city.setValue("Los Angeles");
        await zipcode.setValue("59000");
        await birthDate.setValue(new Date());
        await password.setValue("test");
        await confirmPassword.setValue("test");
        await marketing.setValue(true);

        expect(form.checkValidity()).toBe(true);
      });
    });
  });
});
