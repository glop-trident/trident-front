---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Trident 🔱"
  text: "Documentation"
  tagline: This is the trident documentation.
  actions:
    - theme: brand
      text: Get started
      link: /getting-started/installation

features:
  - icon: 🌺
    title: Design with figma
    details: Our site's captivating design powered by Figma delivers a modern, user-friendly experience with meticulous attention to detail
---

