import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Trident",
  description: "trident description",
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: "Guide", link: '/getting-started/installation'},
    ],

    sidebar: [
      {
        text: 'Getting Started',
        items: [
          { text: 'Installation', link: '/getting-started/installation' },
          { text: 'CLI', link: '/getting-started/cli' },
        ]
      },
    ],

    editLink: {
      pattern: 'https://gitlab.com/glop-trident/trident-front/edit/main/docs/:path',
      text: 'Edit this page on GitHub'
    },

    footer: {
      message: 'Released under the MIT License.',
      copyright: 'Copyright © 2023-present Nawfel Senoussi - Antonin Sanzovo - Romain Morel - Ayoub Kenba - Louis Deloffre'
    },

    socialLinks: [
      { icon: 'github', link: 'https://gitlab.com/glop-trident/trident-front/' }
    ],

    search: {
      provider: 'local',
    },

    lastUpdatedText: 'Updated Date'
  }
})
