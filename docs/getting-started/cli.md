# Command Line Interface

::: warning Compatibility Note
This application requires [Node.js](https://nodejs.org/en) version 18+. Please upgrade your node version if you're not up-to-date.
:::

## Dev server

### `vite`

Start `Vite` dev server in the current directory.

```shell
npm run dev
```
## Build

### `vite build`

Build for production.

```shell
npm run build
```

## Preview

### `vite preview`

Locally preview the production build. Do not use this as a production server as it's not designed for it.

```shell
npm run preview
```

## Test

### `vitest`

Start `Vitest` in the current directory. Will enter the watch mode in development environment and run mode in CI automatically.

```shell
npm run test
```

### `vitest --coverage`

Start `Vitest` in the current directory and provide the coverage

```shell
npm run test:coverage
```

### `vitest --ui`

Start `Vitest` in the current directory and provide UI to view and interact with your tests.

```shell
npm run test:ui
```

## Documentation

### `vitepress dev ./docs`

Start `VitePress` dev server using /docs directory

```shell
npm run docs:dev
```

### `vitepress build ./docs`

Build the `VitePress` site for production.

```shell
npm run docs:build
```

### `vitepress preview ./docs`

Locally preview the production build.

```shell
npm run docs:preview
```

## Eslint

### `eslint src/**/*.{vue,ts} test/**/*.ts`

Run `Eslint` + `Prettier`

```shell
npm run lint
```

### `eslint --fix src/**/*.{vue,ts} test/**/*.ts`

Fix all auto-fixable issues

```shell
npm run lint:fix
```