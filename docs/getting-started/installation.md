# Get started with trident

## Installation

1. Clone the repository

```shell
git clone https://gitlab.com/glop-trident/trident-front.git
```

2. Install all dependencies via npm

```shell
npm i
```

3. Run the dev mode

```shell
npm run dev
```

4. Contribute to our website !


